<!DOCTYPE html>
<html>
<head><title>
	
</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="css/text" href="css/style.css">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@300&display=swap" rel="stylesheet">
</head>
<body>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="index.php">
  <img src="images/lg.jpg" style="display: inline-block;">
  <span style="display: inline-block;">Fresh 4 U</span>
</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Products
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="Products/Bakery.html">Bread & Bakery</a>
          <a class="dropdown-item" href="Products/Veggies&Fruits.html">Veggies & Fruits</a>
          <a class="dropdown-item" href="Products/Beverages.html">Beverages</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="AboutUs/Aboutus.html">About Us</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#Cont">Contact Us</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="Login/ls1.html">Login /Sign Up</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="images/veggies.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>From Fresh Vegetables</h5>
        <p>Do You Hate Brocolli Too!!</p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="images/beverages.jpg" alt="Second slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>To Rejuvinating Beverages</h5>
        <p>Will You Share A Drink!!</p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="images/bread.jpg" alt="Third slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>And the Fibrous Freshly Prepared Bread</h5>
        <p>#Oooh.hhh.. Hot Bread Coming UP</p>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<section class="my-5">
  <div class="py-5">
    <h2 class="text-center">About Us</h2>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-12">
        <img src="images/team.jpg" class="img-fluid aboutimg">
      </div>
      <div class="col-lg-6 col-md-6 col-12">
        <h2 class="display-4">Our collective efforts</h2>  
        <p class="py-3">
          We have been always trying to provide our customers with the best of the best quality essentials from Dairy products to Healthy vegetables.
        </p>
        <a href="AboutUs/Aboutus.html" class="btn btn-success">Check More</a>
      </div>
    </div>
  </div>
</section>
<section class="my-5" id="Prod">
  <div class="py-5">
    <h2 class="text-center">Products</h2>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-12">
        <div class="card">
          <img class="card-img-top" src="images/veggies.jpg" alt="Veggies&Fruits">
          <div class="card-body">
            <h4 class="card-title">Vegetables</h4>
            <p class="card-text">Handpicked and delivered Fresh :)</p>
            <a href="Products/Veggies&Fruits.html" class="btn btn-primary">See Veggies & Fruits</a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-12">
        <div class="card">
          <img class="card-img-top" src="images/beverages.jpg" alt="Beverages">
          <div class="card-body">
            <h4 class="card-title">Beverages</h4>
            <p class="card-text">Mhmm....Quite Amongst the best !!</p>
            <a href="Products/Beverages.html" class="btn btn-primary">See Beverages</a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-12">
        <div class="card">
          <img class="card-img-top" src="images/bread.jpg" alt="Bread&Bakery">
          <div class="card-body">
            <h4 class="card-title">Bread And Bakery</h4>
            <p class="card-text">Fresh Made, Fresh Served ;)</p>
            <a href="Products/Bakery.html" class="btn btn-primary">See Bakery</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="my-5" id="Cont">
  <div class="py-5">
    <h2 class="text-center">Contact Us</h2>
  </div>
  <div class="w-50 m-auto">
    <form action="userinfo.php" method="post">
      <div class="form-group">
        <label>Username</label>
        <input type="text" name="user" autocomplete="off" class="form-control">
      </div>
      <div class="form-group">
        <label>E-Mail</label>
        <input type="text" name="email" autocomplete="off" class="form-control">
      </div>
      <div class="form-group">
        <label>Mobile</label>
        <input type="text" name="mobile" autocomplete="off" class="form-control">
      </div>
      <div class="form-group">
        <label>Comments</label>
        <textarea class="form-control" name="comments">
        </textarea>
      </div>
      <button type="submit" class="btn btn-success">Submit</button>
    </form>
  </div>
</section>
<footer>
  <div class="pd-3 bg-dark text-white text-center">
    <h3>Fresh 4 U </h3>
    <p>© 2020 Fresh 4 U. All Rrights Reserved.</p>
  </div>
</footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>